package pucrs.myflight.modelo;

import java.time.Duration;
import java.time.LocalDateTime;
import java.util.ArrayList;

public class VooEscalas extends Voo {

	private ArrayList<Rota> rotas;

	public VooEscalas(Rota rota, LocalDateTime datahora, Duration duracao) {
		super(rota, datahora, duracao);
		//this.rota = rota;
		this.rotas = new ArrayList<>();
		//this.datahora = datahora;
		//this.duracao = duracao;
		//this.status = Status.CONFIRMADO; // default é confirmado
	}
	
	public void adicionarRota(Rota r) {
		rotas.add(r);
	}
	
	@Override
    public String toString() {
		StringBuilder aux = new StringBuilder();
		aux.append(super.toString());
		for(Rota r: rotas)
				aux.append("\n    "+r);
		return aux.toString();
       //return getStatus() + " " + getDatahora() +
       //	   "("+getDuracao()+"):\n" + "    "+getRota()
       //	   +"\n    "+rotaFinal;
    }
	
	

}